
//This is the file ,where all the test cases regarding the get calls ,
//in this file you can edit ,add or modify the test cases . 
//To run the file you need terminal , which you can access is from Terminal option .
//To the run the test cases you need to use the command "npm run test" and it will run all the test cases for you .

//import "jest-allure/dist/setup";

import React from 'react';

const config = require('./config');

 require('dotenv').config();
 const dotenv = require('dotenv');
  
const Url = process.env.REACT_APP_API_ENDPOINT;
var ClientId = process.env.REACT_APP_CLIENT_ID;
var secret = process.env.REACT_APP_SECRETT;
var contentType = process.env.REACT_APP_CONTENT_TYPE;


const supertest = require("supertest");
const request = supertest(Url);


describe(" health check for the clock " , () => {
  it("connection with the clock is up and running ", async () => {
    const response = await request.get("/system/v1/clock")
   
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}'); 



   expect(response.statusCode).toBe(200);
   


  });
  it("Status check for the clock ", async () => {
    const response = await request.get("/system/v1/clock")
   
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');

    expect(response.body.status).toBe('ok');

  })
});

describe(" health check for the clock " , () => {
  it("This test will ping the clock and get the response back", async () => {
    const response = await request.get("/system/v1/ping")
    
   expect(response.statusCode).toBe(200);

  });
  it("this will get time ", async () => {
    const response = await request.get("/system/v1/ping")
    

   expect(response.body).toBe("ok");
  });

});
describe(" health check for the clock " , () => {
  it("this will check the db connection and get the reposne back", async () => {
    const response = await request.get("/system/v1/pool")
    
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);

  });
  it("this test will give us the maximum number of connection ", async () => {
    const response = await request.get("/system/v1/pool")
    
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');

   expect(response.body.maxConnections).toBe(300);

  });
  it("this test will get us the number of current connection ", async () => {
    const response = await request.get("/system/v1/pool")
   
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.body.currentConnections).toBe(5);

  });
  it("this test will get us the number of availabe connection", async () => {
    const response = await request.get("/system/v1/pool")
   
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');

   expect(response.body.availableConnections).toBe(5);

  })
});
describe(" health check for the clock " , () => {
  it("is it done", async () => {
    const response = await request.get("/system/v1/version")
    
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');

   expect(response.statusCode).toBe(200);

  });
  
  it("this test will get us the version of the application ", async () => {
    const response = await request.get("/system/v1/version")
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
    for(let i=0;i<response.body.data.length;i++){	            
      await(console.log(response.body.data[0].version));	          
       // await (console.log(response.body.data[0].applicationName));
    }
    
  })

  
  
});
describe(" health check for the core API " , () => {
  it("This test will get us the report config", async () => {
    const response = await request.get("/core/v1/file/list")
  
    .set(config.commanHeader)
     .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}')

  

   expect(response.statusCode).toBe(200);
  });
  it("This test will get us the report config", async () => {
    const response = await request.get("/core/v1/reportConfig ")
    .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
    

   expect(response.statusCode).toBe(200);
  })
});
describe("Model API's " , () => {
  it("this test will return 20 accounts ", async () => {
    const response = await request.get("/core/v1/file/list/ach")
    .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
  // console.log(response)

  });
  it("this will get time ", async () => {
    const response = await request.get("/core/v1/file/list/ach")
    .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
    expect(response.body).toBe()
})

});
describe(" Model  API's  " , () => {
  it(" This test will get us the configured test account response ", async () => {
    const response = await request.get("/model/v1/acctType")
    .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
  })
});
describe(" Model  API's " , () => {
  it("This test will get us the configured account segment ", async () => {
    const response = await request.get("/model/v1/acctgSeg")
    .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
  })
});
describe(" Model  API's  " , () => {
  it("this test will get us the transaction limit", async () => {
    const response = await request.get("/model/v1/accumTrnLimit")
    .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
  })
});

describe(" Model  API's " , () => {
  
  it("this test will give us the transaction codes ", async () => {
    const response = await request.get("/model/v1/trnCode")
    .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
 })

});

  
    
  
  describe(" Model  API's  " , () => {
    it("This test will get the bank paramaeters API response", async () => {
      const response = await request.get("/model/v1/bankparam")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get the US bank paramaeters API response ", async () => {
      const response = await request.get("/model/v1/bankparamUSBankInfo")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's  " , () => {
    it("This test will get the batch API response", async () => {
      const response = await request.get("/model/v1/batch ")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will give us the listing of configured calenders", async () => {
      const response = await request.get("/model/v1/calendar ")
      .set(config.commanHeader)
      .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("this will get time ", async () => {
      const response = await request.get("/model/v1/ccyCode ")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("this will get time ", async () => {
      const response = await request.get("/model/v1/cntry ")
      .set(config.commanHeader)
      .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("this will get time ", async () => {
      const response = await request.get("/model/v1/holdPosn")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get configured matrixes in the system ", async () => {
      const response = await request.get("/model/v1/matrix")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get configured financial institutions ", async () => {
      const response = await request.get("/model/v1/party_org_fininst")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's  " , () => {
    it("This test will get a product and save one to variable to use later  ", async () => {
      const response = await request.get("/model/v1/prod_bk")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get the list of GL", async () => {
      const response = await request.get("/model/v1/posn_gl")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's  " , () => {
    it("This test will get the transaction codes for the environment", async () => {
      const response = await request.get("/model/v1/trnCode")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get date and time of the transactions ", async () => {
      const response = await request.get("/model/v1/trn")
      .set(config.commanHeader)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });




  


