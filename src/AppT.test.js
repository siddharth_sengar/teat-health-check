
//This is the file ,where all the test cases regarding the get calls ,
//in this file you can edit ,add or modify the test cases . 
//To run the file you need terminal , which you can access is from Terminal option .
//To the run the test cases you need to use the command "npm run test" and it will run all the test cases for you .


const config = require('./config');

 const expect = require('expect');
 
 require('dotenv').config();
 

const testUrl = process.env.TEST_BASE_URL
const trainingENV = process.env.NODE_ENV;
const testingENV = process.env.NODE_ENV_TEST;

const configtest = require('./confitsta');


const supertest = require("supertest");
const request = supertest(testUrl);

 
 

if(process.env.NODE_ENV === trainingENV){
  
 
console.log("Testing ")


 }



describe(" health check for the clock " , () => {
  it("connection with the clock is up and running ", async () => {
    const response = await request.get("/system/v1/clock")
    //.set(config.authorization.client_id)
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}'); 


   expect(response.statusCode).toBe(200);
   


  });
  it("Status check for the clock ", async () => {
    const response = await request.get("/system/v1/clock")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');

    expect(response.body.status).toBe('ok');

  });
  
  
  
});
describe(" health check for the clock " , () => {
  it("This test will ping the clock and get the response back", async () => {
    const response = await request.get("/system/v1/ping")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);

  });
  
});
describe(" health check for the clock " , () => {
  it("this will check the db connection and get the reposne back", async () => {
    const response = await request.get("/system/v1/pool")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);

  });
  it("this test will give us the maximum number of connection ", async () => {
    const response = await request.get("/system/v1/pool")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');

   expect(response.body.maxConnections).toBe(300);

  });
  it("this test will get us the number of current connection ", async () => {
    const response = await request.get("/system/v1/pool")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.body.currentConnections).toBe(5);

  });
  it("this test will get us the number of availabe connection", async () => {
    const response = await request.get("/system/v1/pool")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');

   expect(response.body.availableConnections).toBe(5);

  })
});
describe(" health check for the clock " , () => {
  it("this will get time ", async () => {
    const response = await request.get("/system/v1/version")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);

  });
 
  it("this test will get us the version of the application ", async () => {
    const response = await request.get("/system/v1/version")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
    for(let i=0;i<response.body.data.length;i++){	            
      await(console.log(response.body.data[0].version));	          
       // await (console.log(response.body.data[0].applicationName));
    }
    
  })

  
  
});
describe(" health check for the core API " , () => {
  it("This test will get us the report config", async () => {
    const response = await request.get("/core/v1/file/list")
    .set(configtest.commanHeadertest)
 
     .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}')

  //  // }
  

   expect(response.statusCode).toBe(200);
  });
  it("This test will get us the report config", async () => {
    const response = await request.get("/core/v1/reportConfig ")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
    console.log(config.commanHeader);

   expect(response.statusCode).toBe(200);
  })
});
describe("Model API's " , () => {
  it("this test will return 20 accounts ", async () => {
    const response = await request.get("/core/v1/file/list/ach")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
  

  });
  it("this will get time ", async () => {
    const response = await request.get("/core/v1/file/list/ach")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
    expect(response.body).toBe()
})

});
describe(" Model  API's  " , () => {
  it(" This test will get us the configured test account response ", async () => {
    const response = await request.get("/model/v1/acctType")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
  })
});
describe(" Model  API's " , () => {
  it("This test will get us the configured account segment ", async () => {
    const response = await request.get("/model/v1/acctgSeg")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
  })
});
describe(" Model  API's  " , () => {
  it("this test will get us the transaction limit", async () => {
    const response = await request.get("/model/v1/accumTrnLimit")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
  })
});

describe(" Model  API's " , () => {
  
  it("this test will give us the transaction codes ", async () => {
    const response = await request.get("/model/v1/trnCode")
    .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
   expect(response.statusCode).toBe(200);
 })

});

  
    
  
  describe(" Model  API's  " , () => {
    it("This test will get the bank paramaeters API response", async () => {
      const response = await request.get("/model/v1/bankparam")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get the US bank paramaeters API response ", async () => {
      const response = await request.get("/model/v1/bankparamUSBankInfo")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's  " , () => {
    it("This test will get the batch API response", async () => {
      const response = await request.get("/model/v1/batch ")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will give us the listing of configured calenders", async () => {
      const response = await request.get("/model/v1/calendar ")
      .set(configtest.commanHeadertest)
      .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("this will get time ", async () => {
      const response = await request.get("/model/v1/ccyCode ")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("this will get time ", async () => {
      const response = await request.get("/model/v1/cntry ")
      .set(configtest.commanHeadertest)
      .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("this will get time ", async () => {
      const response = await request.get("/model/v1/holdPosn")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get configured matrixes in the system ", async () => {
      const response = await request.get("/model/v1/matrix")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get configured financial institutions ", async () => {
      const response = await request.get("/model/v1/party_org_fininst")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's  " , () => {
    it("This test will get a product and save one to variable to use later  ", async () => {
      const response = await request.get("/model/v1/prod_bk")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get the list of GL", async () => {
      const response = await request.get("/model/v1/posn_gl")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's  " , () => {
    it("This test will get the transaction codes for the environment", async () => {
      const response = await request.get("/model/v1/trnCode")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
  
     expect(response.statusCode).toBe(200);
    })
  });
  describe(" Model  API's " , () => {
    it("This test will get date and time of the transactions ", async () => {
      const response = await request.get("/model/v1/trn")
      .set(configtest.commanHeadertest)
    .set('Fnx-Header', '{"identity":{"userRoles":["developer"]}}');
     expect(response.statusCode).toBe(200);
    })
  });




  


